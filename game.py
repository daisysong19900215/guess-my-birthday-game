from random import randint
guess_number = 1
guess_month_number = randint(1, 12)
guess_year_number = randint(1924, 2004)
name = input(“Hi! What is your name?“)
print(name, “were you born in: “, guess_month_number, “/”, guess_year_number, “?”)
answer = input(“yes or no?“)
guess_number += 1
if answer.lower() == “yes”:
   print(“I knew it”)
   exit()
if answer.lower() == “no” and guess_number < 5:
     print(“Drat! Lemme try again!“)
     print(name, “were you born in: “, guess_month_number, “/”, guess_year_number, “?”)
     answer = input(“yes or no?“)
if guess_number == 5:
        print(“I have other things to do. Good bye.“)
        exit()
